#!/bin/bash

# A simple script to display wifi name
# 'ssid.sh -i' = with icon, 'ssid.sh' = text only

SSID_NAME=$(/usr/sbin/iwgetid -r)

if [[ "${SSID_NAME}" != "" ]]; then
	if  [[ $1 = "-i" ]]; then
    echo "  ${SSID_NAME}"
	else
    echo "${SSID_NAME}"
	fi
else
    echo "Assente"
fi

exit
