#!/bin/bash

PCTL=$(playerctl status)

if [[ ${PCTL} == "" ]]; then
  echo "Musica assente"
elif [[ ${PCTL} == "Stopped" ]]; then
  echo "Riproduzione arrestata"
elif [[ ${PCTL} == "Paused" ]]; then
  echo "Riproduzione in pausa"
else
  echo "Stai ascoltando"
fi

exit

